var express = require('express');
var bodyParser = require('body-parser');
var path = require('path');

var port = process.env.PORT || 9000;
var app = express();

app.use(bodyParser.json());
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    
    res.setHeader('Content-Type', 'application/json');
    next();
});

app.use(express.static(path.resolve(__dirname, '..', 'build')));

// routing
app.use(require('./components/quiz/quiz.router.js'));
//

app.get('*', (req, res) => {
    res.sendFile(path.resolve(__dirname, '..', 'build', 'index.html'));
})

// app.use(function(err, req, res, next){
//     console.log(err);
//     res.status(400).json(err);
// });

//if (app.get('env') === 'development') {

//   app.use(function(err, req, res, next) {
//     res.status(err.status || 500);
//     res.json({
//         message: err.message,
//         error: err
//     });
//   });

//}

// app.use(function(err, req, res, next) {
//     res.status(err.status || 500);
//     res.render('error', {
//         message: err.message,
//         error: {}
//     });
// });

app.listen(port, () => {
    console.log('listening on port', port);
})