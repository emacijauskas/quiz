var Joi = require('joi');

module.exports = { 
    body: {
        username: Joi.string().min(3).max(25).required(),
        quizId: Joi.string().required(),
        answers: Joi.array().required().min(1)
    }
}
