var mongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
var assert = require('assert');

var serverAddress = 'mongodb://localhost:27017/quiz'

var repository = {
    insertUserResult: function(result, username, quizId, callback) {
        mongoClient.connect(serverAddress, function(err, db) {
            assert.equal(null, err);
            db.collection('results').insertOne({ result: result, username: username, quizId: quizId }, function(err, result) {
                if(err) {
                    callback(err);
                } 
            })
            db.close();
        })
    },

    findQuizById: function(id, callback) {
        mongoClient.connect(serverAddress, function(err, db) {
            assert.equal(null, err);
            db.collection('quizes').findOne({"_id" : ObjectId(id)}, { "_id": true, "questions": true, "title": true }, function(err, result) {
                if(err) {
                    callback(err, null);
                } 
                
                if(result) {
                    callback(null, result);
                }
            })
            db.close();
        })
    },

    findQuizAnswers: function(id, callback) {
        mongoClient.connect(serverAddress, function(err, db) {
            assert.equal(null, err);
            db.collection('quizes').findOne({"_id" : ObjectId(id)}, { 'correctAnswers': true }, function(err, result) {
                if(err) {
                    callback(err, null);
                } 
                
                if(result) {
                    callback(null, result);
                }
            })
            db.close();
        })
    },

    findAllQuizes: function(callback) {
        mongoClient.connect(serverAddress, function(err, db) {
            assert.equal(null, err);
            db.collection('quizes').find({}, {'_id': true, 'title': true}).toArray(function(err, results) {
                if(err) {
                    callback(err, null);
                } 
                
                if(results) {
                    callback(null, results);
                }
            })
            db.close();
        })
    },

    insertNewQuiz: function(quiz, callback) {
        mongoClient.connect(serverAddress, function(err, db) {
            assert.equal(err, null);
            db.collection('quizes').insertOne(initialQuizJson, function(err, result) {
                console.log("Quiz successfully inserted");
                callback();
            });
            db.close();
        })
    }
};

module.exports = repository;