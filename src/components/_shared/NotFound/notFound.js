import React, { Component } from 'react';
import { Link } from 'react-router';

import './notFound.css';

export default class NotFound extends Component {
    render() {
        return (
            <div className="NotFound-container">
                <h2>404 jesus not found</h2>
                <Link to="/">Go home</Link>
            </div>
        )
    }
}