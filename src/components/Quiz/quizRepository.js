var quizRepository = {
    getQuizById: function(quizId) {
        return fetch(`http://localhost:9000/quiz/${quizId}`)
            .then((response) => response.json())
            .catch((error) => { console.log(error); })
    },

    submitAnswers: function(answers) {
        return fetch('http://localhost:9000/result', { 
            method: 'POST',
            headers: { 'Content-Type': 'application/json', 'Accept': 'application/json' }, 
            body: JSON.stringify(answers)
        }).then((response) => response.json())
        .catch((error) => console.log(error))
    },

    getAllQuizes: function() {
        return fetch('http://localhost:9000/quiz/list')
            .then((response) => response.json())
            .catch((error) => { console.log(error); })
    }
}

export { quizRepository as default };